# Lucidpress DAM Integrations

Provide service with plugin system to generate JSON with Lucidpress DAM Integration API data.

## Example usage

Run `drush lucidpress_dam:gen example` this generate drupal file for Lucidpress DAM Integration on path `/sites/default/files/lucidpress_dam/example.json`

## Documentation

- [Lucidpress DAM API reference](https://help.lucidpress.com/dam-integration-api)
