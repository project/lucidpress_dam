<?php

namespace Drupal\lucidpess_dam_netx;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\lucidpress_dam\Objects\LucidpressFolder;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * Service description.
 */
class Folders {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The netx assets getter.
   *
   * @var \Drupal\lucidpess_dam_netx\Assets
   */
  protected $assetService;

  /**
   * Lucidpress DAM Netx settings.
   *
   * @var \Drupal\Core\Config\config
   */
  protected $settings;

  /**
   * Api url.
   *
   * @var string
   */
  protected $apiUrl;

  /**
   * Constructs a Folders object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The Logger.
   * @param \Drupal\lucidpess_dam_netx\Assets $assetService
   *   The netx assets getter.
   */
  public function __construct(ClientInterface $client, ConfigFactoryInterface $config_factory, LoggerInterface $logger, Assets $assetService) {
    $this->client = $client;
    $this->configFactory = $config_factory;
    $this->logger = $logger;
    $this->assetService = $assetService;
    $this->settings = $this->configFactory->get(NetxInterface::CONFIG_NAME);
    $this->options = [
      'headers' => [
        'Authorization' => 'apiToken ' . $this->settings->get('token'),
        'Content-Type' => 'application/json',
      ],
    ];
    $this->apiUrl = $this->settings->get('api_url') . '/api/rpc';
  }

  /**
   * Get info about folder.
   *
   * @param int $id
   *   The id of netx folder.
   *
   * @return object
   *   Request result.
   */
  public function folderInfo(int $id): object {
    $requestBody = NetxInterface::REQUETS_BODY;
    $requestBody['method'] = 'getFolderById';
    $requestBody['params'][] = $id;
    $requestBody['params'][] = [
      'data' => [
        "folder.id",
        "folder.base",
        "folder.children",
      ],
    ];
    $response = $this->client->request(
        'POST',
        $this->apiUrl,
        array_merge($this->options, ['json' => $requestBody])
    );
    $body = $response->getBody();
    $contents = $body->getContents();
    $data = json_decode($contents);
    return $data?->result;
  }

  /**
   * Get info about folder.
   *
   * @param int $id
   *   The id of netx folder.
   * @param int $request_size
   *   The number of folders in response from api.
   *
   * @return array
   *   Request result.
   */
  public function getFoldersByParrent(int $id, int $request_size): array {
    $requestBody = NetxInterface::REQUETS_BODY;
    $requestBody['method'] = 'getFoldersByParent';
    // Set parent folder id.
    $requestBody['params'][] = $id;
    $requestBody['params'][] = [
      'page' => [
        'startIndex' => 0,
        'size' => $request_size,
      ],
      'data' => [
        "folder.id",
        "folder.base",
        "folder.children",
      ],
    ];
    $response = $this->client->request(
      'POST',
      $this->apiUrl,
      array_merge($this->options, ['json' => $requestBody])
    );
    $body = $response->getBody();
    $contents = $body->getContents();
    $data = json_decode($contents);
    return $data?->result?->results;
  }

  /**
   * Recursivelly get folders structure and assets.
   */
  public function foldersStructure(int $id = 0, int $countFolders = 0, int $depth = 0, int $countAssets = 0) {
    $parrent_info = $this->folderInfo($id);
    $folderDepthLevel = $this->settings->get('folder_depth_level');
    $children = $this->getFoldersByParrent($id, $parrent_info?->children?->folderCount);
    $assets = [];
    if ($parrent_info?->children->assetCount > 0) {
      $assets = $this->assetService->getImagesByFolder($id);
      $countAssets += count($assets);
    }
    $folder = new LucidpressFolder($parrent_info->id, $parrent_info->name, [], $assets);
    $countFolders += 1;
    foreach ($children as $child) {
      $countFolders += 1;
      if ($child?->children?->folderCount > 0 && $depth < $folderDepthLevel) {
        $folderWithChildrens = $this->foldersStructure($child->id, 0, $depth + 1, 0);
        $folder->addFolder($folderWithChildrens['folder']);
        $countFolders += $folderWithChildrens['count_folders'];
        $countAssets += $folderWithChildrens['count_assets'];
        continue;
      }
      $assets = [];
      if ($child?->children?->assetCount > 0) {
        $assets = $this->assetService->getImagesByFolder($child->id);
        $countAssets += count($assets);
      }
      $folder->addFolder(new LucidpressFolder($child->id, $child->name, [], $assets));
    }
    $msg = 'Folder %name created. Number of children folders: %c';
    $this->logger->debug($msg, [
      '%name' => $parrent_info->name,
      '%c' => $parrent_info?->children?->folderCount,
    ]);
    return [
      'folder' => $folder,
      'count_folders' => $countFolders,
      'count_assets' => $countAssets,
    ];
  }

  /**
   * Get lucidperess DAM structure.
   *
   * @return \Drupal\lucidpress_dam\Objects\LucidpressFolder
   *   The Lucidpress DAM folder object.
   */
  public function getFolderStructure(): LucidpressFolder {
    // Init assets.
    $this->logger->info('Start generate folders structure for Lucidpress DAM');
    $this->assetService->init();
    // Generate lucidpress dam structure.
    $id = $this->settings->get('folder_id');
    $structure = $this->foldersStructure($id);
    $msg = 'Folders structure with images generated. Generated %i images in %f folders.';
    $this->logger->info($msg, [
      '%f' => $structure['count_folders'],
      '%i' => $structure['count_assets'],
    ]);
    return $structure['folder'];
  }

}
