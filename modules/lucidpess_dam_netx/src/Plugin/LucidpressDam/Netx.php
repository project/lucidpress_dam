<?php

namespace Drupal\lucidpess_dam_netx\Plugin\LucidpressDam;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\lucidpess_dam_netx\Folders;
use Drupal\lucidpress_dam\LucidpressDamPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the lucidpress_dam.
 *
 * @see \Drupal\lucidpress_dam\LucidpressDamPluginBase
 * @LucidpressDam(
 *   id = "netx",
 *   label = @Translation("Netx"),
 *   description = @Translation("Netx description.")
 * )
 */
class Netx extends LucidpressDamPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Lucidpress DAM NetX Folders service.
   *
   * @var \Drupal\lucidpess_dam_netx\Folders
   */
  protected $folders;

  /**
   * Constructs the Netx class.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\lucidpess_dam_netx\Folders $folders
   *   The Lucidpress DAM NetX Folders service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Folders $folders) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->folders = $folders;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('lucidpess_dam_netx.folders')
    );
  }

  /**
   * Get data from netx.
   */
  public function getData() {
    return array_values($this->folders->getFolderStructure()->folders);
  }

}
