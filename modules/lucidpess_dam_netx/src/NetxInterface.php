<?php

namespace Drupal\lucidpess_dam_netx;

/**
 * Netx defaults.
 */
interface NetxInterface {
  const CONFIG_NAME = 'lucidpess_dam_netx.settings';
  const REQUETS_BODY = [
    "id" => "13576991614322",
    "method" => "",
    "params" => [],
    "jsonrpc" => "2.0",
  ];

}
