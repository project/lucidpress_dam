<?php

namespace Drupal\lucidpess_dam_netx;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use Drupal\lucidpress_dam\Objects\LucidpressImage;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\Utils;
use Psr\Log\LoggerInterface;

/**
 * Service description.
 */
class Assets {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Lucidpress DAM Netx settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $settings;

  /**
   * Api url.
   *
   * @var string
   */
  protected $apiUrl;

  /**
   * Lucidpress DAM images.
   *
   * @var array
   */
  protected $lucidpressDamImages;

  /**
   * NetX assets.
   *
   * @var array
   */
  protected $netxAssets;

  /**
   * Constructs an Assets object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The Logger.
   */
  public function __construct(ClientInterface $client, ConfigFactoryInterface $config_factory, LoggerInterface $logger) {
    $this->client = $client;
    $this->configFactory = $config_factory;
    $this->logger = $logger;
    $this->settings = $this->configFactory->get(NetxInterface::CONFIG_NAME);
    $this->options = [
      'headers' => [
        'Authorization' => 'apiToken ' . $this->settings->get('token'),
        'Content-Type' => 'application/json',
      ],
    ];
    $this->apiUrl = $this->settings->get('api_url') . '/api/rpc';
    $this->lucidpressDamImages = [];
    $this->netxAssets = [];
  }

  /**
   * Init assests service to get and convert netx assests to LucidpressImages.
   */
  public function init() {
    $this->fetchAssets();
    $this->convertNetxToLucidpress();
  }

  /**
   * Generate async request to get netx assets.
   *
   * @param int $id
   *   The netx folder id.
   * @param int $startIndex
   *   Start number to get elements.
   * @param int $size
   *   Numbers of elements.
   *
   * @return \GuzzleHttp\Promise\PromiseInterface
   *   The Guzzle clien promise.
   */
  private function asyncGetAssettsByFolderId(int $id, int $startIndex, int $size): PromiseInterface {
    $requestBody = NetxInterface::REQUETS_BODY;
    $requestBody['method'] = 'getAssetsByFolder';
    $requestBody['params'][] = $id;
    // Get assets from all internal directories.
    $requestBody['params'][] = TRUE;
    $requestBody['params'][] = [
      'page' => [
        'startIndex' => $startIndex,
        'size' => $size,
      ],
      'data' => [
        "asset.id",
        "asset.base",
        "asset.proxies",
        "asset.attributes",
        "asset.folders",
      ],
    ];
    return $this->client->requestAsync(
      'POST',
      $this->apiUrl,
      array_merge($this->options, ['json' => $requestBody])
    );
  }

  /**
   * Fetch netx assets by folder id.
   */
  private function fetchAssets(): void {
    $msg = 'Assets fetching is started.';
    $this->logger->debug($msg);

    $id = $this->settings->get('folder_id');
    $responce = $this->asyncGetAssettsByFolderId($id, 0, 1)->wait();
    $body = $responce->getBody();
    $contents = $body->getContents();
    $info = json_decode($contents);
    $size = $info?->result?->size;
    // Numbers of future request.
    $responce_size = $this->settings->get('assets_response_size');
    $requestsNumbers = intdiv($size, $responce_size) + 1;
    $curentStartIndex = 0;
    $msg = 'Assets in folders: %s, Requests to process: %r.';
    $this->logger->info($msg, ['%s' => $size, '%r' => $requestsNumbers]);
    // Fetch netx assets in parallel mode.
    $paralel_requests = $this->settings->get('paralel_requests');
    for ($r = 0; $r < $requestsNumbers; $r += $paralel_requests) {
      if ($curentStartIndex >= $size) {
        break;
      }
      $promises = [];
      for ($i = 0; $i < $paralel_requests; $i++) {
        $promises[] = $this->asyncGetAssettsByFolderId($id, $curentStartIndex, $responce_size);
        $curentStartIndex += $responce_size;
      }
      // Wait for promises.
      Utils::settle($promises)->wait();
      $msg = 'Number of requests completed: %r, Number of assets fetched: %a';
      $this->logger->debug($msg, [
        '%r' => $r + $i,
        '%a' => $curentStartIndex,
      ]);
      // Unpack promises.
      foreach ($promises as $promise) {
        // Get responce from promise.
        $responce = $promise->wait();
        // Convert responce to netx objects.
        $body = $responce->getBody();
        $contents = $body->getContents();
        $decoded_contents = json_decode($contents);
        if (!empty($decoded_contents?->result?->results)) {
          $this->netxAssets = array_merge(
            $this->netxAssets,
            $decoded_contents->result->results
          );
        }
      }
    }
    $msg = 'Assets fetching is done.';
    $this->logger->debug($msg);
  }

  /**
   * Convert Netx assets to Lucidpress DAM Images.
   */
  private function convertNetxToLucidpress(): void {
    // Convert netx assets objects to lucidpress dam image object.
    foreach ($this->netxAssets as $asset) {
      // Genarate lucidpress dam fileUrl attribute from netx filename and id.
      $proxyUrl = $this->settings->get('netx_proxy_url');
      $fileUrl = $proxyUrl . 'file/' . (string) $asset->id . '/' . urlencode($asset->fileName);
      $thumbnail = '';
      // Generate lucidpress dam thumbnailUrl attribute from netx proxies.
      foreach ($asset?->proxies as $proxie) {
        if ($proxie->name == 'Thumbnail') {
          $thumbnail = $proxyUrl . 'thumbnail/' . (string) $asset->id . '/' . urlencode($proxie->file->name);
        }
      }
      // Generate lucidpress image tags attribute from netx attributes.
      $tags = [];
      foreach ($asset?->attributes as $key => $attribute) {
        foreach ($attribute as $tag) {
          $tags[] = $key . ' ' . $tag;
        }
      }
      // Sort images by folder id.
      $folders = $asset?->folders;
      $folderId = reset($folders)->id;
      $this->lucidpressDamImages[$folderId][$asset->id] = new LucidpressImage(
        $asset->id,
        $asset->name,
        $fileUrl,
        empty($thumbnail) ? $fileUrl : $thumbnail,
        $tags
      );
    }
  }

  /**
   * Get imeges for folder by ID.
   *
   * @param int $id
   *   The folder id.
   *
   * @return array
   *   Return array of LucidpressImage when folder has images.
   */
  public function getImagesByFolder(int $id): array {
    if (isset($this->lucidpressDamImages[$id])) {
      return $this->lucidpressDamImages[$id];
    }
    return [];
  }

}
