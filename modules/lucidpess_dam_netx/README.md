# Lucidpess DAM NetX

Add to your settings.php next lines:

```php
Add to your config.local.php next line:
$config['lucidpess_dam_netx.settings']['token'] = '<Your NetX API Token>';
$config['lucidpess_dam_netx.settings']['netx_proxy_url'] = '<Your NetX proxy URL>';
```

Run `drush -vvv lucidpress_dam:gen netx` to generate JSON file.
