<?php

namespace Drupal\netx_proxy\Controller;

use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for NetX proxy routes.
 */
class NetxProxyController extends ControllerBase {

  /**
   * Get mime type by file name.
   */
  private function mimeTypes($filename) {
    $mime_types = $this->config('netx_proxy.settings')->get('mime_types');

    $ext = explode('.', $filename);
    $ext = strtolower(end($ext));

    if (array_key_exists($ext, $mime_types)) {
      return (is_array($mime_types[$ext])) ? $mime_types[$ext][0] : $mime_types[$ext];
    }

    return 'application/octet-stream';
  }

  /**
   * Get asses from NetX.
   *
   * @param string $url
   *   The Url of asset.
   *
   * @return string|false
   *   The file steam.
   */
  private function getAsset(string $url): string|FALSE {
    $token = $this->config('lucidpess_dam_netx.settings')->get('token');
    $url = $this->config('lucidpess_dam_netx.settings')->get('api_url') . $url;
    $opts = ['http' => ['header' => "Authorization:apiToken " . $token . "\r\n"]];
    $context = stream_context_create($opts);
    return file_get_contents($url, FALSE, $context);
  }

  /**
   * Get asset image from NetX.
   *
   * @param mixed $id
   *   The asset id.
   * @param string $file
   *   The asset file name.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   File Responce.
   */
  public function getFile($id, $file): Response {
    $url = '/api/file/asset/' . (string) $id . '/' . urlencode($file);
    $img = $this->getAsset($url);
    if (!$img) {
      return new Response('Asset no found', 404);
    }
    $headers = [
      'Content-Type' => $this->mimeTypes($file),
    ];
    return new Response($img, 200, $headers);
  }

  /**
   * Get asset Thumbnail from NetX.
   *
   * @param mixed $id
   *   The asset id.
   * @param string $file
   *   The asset file name.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   File Responce.
   */
  public function getThumbnail($id, $file) {
    $url = '/api/file/asset/' . (string) $id . '/thumbnail';
    $img = $this->getAsset($url);
    if (!$img) {
      return new Response('Asset no found', 404);
    }
    $headers = [
      'Content-Type' => $this->mimeTypes($file),
    ];
    return new Response($img, 200, $headers);
  }

}
