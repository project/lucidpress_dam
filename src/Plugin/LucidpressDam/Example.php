<?php

namespace Drupal\lucidpress_dam\Plugin\LucidpressDam;

use Drupal\lucidpress_dam\LucidpressDamPluginBase;

/**
 * Plugin implementation of the lucidpress_dam.
 *
 * @see \Drupal\lucidpress_dam\LucidpressDamPluginBase
 * @LucidpressDam(
 *   id = "example",
 *   label = @Translation("Example"),
 *   description = @Translation("Example description.")
 * )
 */
class Example extends LucidpressDamPluginBase {

}
